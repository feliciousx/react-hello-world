import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
  const className = `square${props.highlight ? ' highlight' : ''}`
  return (
    <button className={className} onClick={props.onClick}>
      {props.value}
    </button>
  );
}

function Board(props) {
  function renderSquare(i) {
    return (
      <Square
        key={i}
        value={props.squares[i]}
        onClick={() => props.onClick(i)}
        highlight={props.winSquares.includes(i)}
      />
    );
  }

  const squares = []
  for (let r = 0; r < props.height; r++) {
    const row = [];

    for (let c = 0; c < props.width; c++) {
      row.push(renderSquare(r * props.height + c));
    }

    squares.push(<div key={r} className="board-row">{row}</div>)
  }

  return (
    <div>{squares}</div>
  );
}

class Game extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      history: [{
        squares: Array(props.width * props.height).fill(null),
      }],
      xIsNext: true,
      stepNumber: 0,
      isAscending: true,
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();

    if (calculateWinner(squares).winner !== null || squares[i]) {
      return;
    }

    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat({
        squares: squares,
        location: indexToCoordinate(i)
      }),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    })
  }

  handleSortOrder() {
    this.setState({
      isAscending: !this.state.isAscending
    })
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const { winner, line, isDraw } = calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      const txt = move ?
        `Go to move #${move} (${step.location.x}, ${step.location.y})` :
        'Go to game start';

      const desc = move === this.state.stepNumber ? <strong>{txt}</strong> : txt

      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      )
    })


    const status = winner ?  `Winner: ${winner}`:
      isDraw ? 'Draw!' : `Next player: ${this.state.xIsNext ? 'X' : 'O'}`;

    return (
      <div className="game">
        <div className="game-board">
          <Board
            width={this.props.width}
            height={this.props.height}
            squares={current.squares}
            winSquares={line}
            onClick={(i) => this.handleClick(i)}/>
        </div>
        <div className="game-info">
          <div>{status}</div>
          <button onClick={() => this.handleSortOrder()}>{this.state.isAscending ? 'Ascending' : 'Descending'}</button>
          <ol>{this.state.isAscending ? moves : moves.reverse()}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game width={3} height={3} />,
  document.getElementById('root')
);

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return {
        winner: squares[a],
        line: lines[i],
        isDraw: false,
      };
    }
  }

  return {
    winner: null,
    line: [],
    isDraw: squares.every(s => s !== null),
  };
}

function indexToCoordinate(i) {
  return {
    x: Math.trunc(i % 3),
    y: Math.trunc(i / 3),
  }
}
